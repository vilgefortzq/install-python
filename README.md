To install python execute script with provided version, for example
sudo ./install.sh 3.4.2

Python will instal itself in /opt/python3.4.3 and create symlink in /bin/py3.4.3, 
so you can use it as executable without providing full path.
