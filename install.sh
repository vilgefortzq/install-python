d=$pwd
wget http://www.python.org/ftp/python/$1/Python-$1.tar.xz
tar xJf ./Python-$1.tar.xz
cd ./Python-$1
./configure --prefix=/opt/python$1
make && sudo make install
ln -s /opt/python$1/bin/python${1%.*} /bin/py$1
cd ../
rm -rf Python-$1
rm -f Python-$1.tar.xz
